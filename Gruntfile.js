module.exports = function(grunt) {

  //dynamically load tasks
  require('matchdep').filterDev( 'grunt-*' ).forEach(grunt.loadNpmTasks);

  grunt.initConfig({

    // Get contents of package.json
    pkg: grunt.file.readJSON('package.json'),

    // Prompt to get Client Name, Project Name, arbitrary questions
    prompt: {
      target: {
        options: {
          questions: [
            {
              config: 'client',
              type: 'input',
              message: 'Client name',
              validate: function(value) {
                if (value === '') {
                  return 'Please enter a client name';
                }
                return true;
              },
              filter:  function(value) {
                // Return Client Name and client-name
                var c = value.split(' '),
                    i = 0,
                    l = c.length,
                    n = [],
                    s = [];
                for (;i < l; i++) {
                  n[i] = c[i].substr(0,1).toUpperCase()+c[i].substr(1);
                  s[i] = c[i].toLowerCase();
                }
                return {
                  "name" : n.join(' '),
                  "string" : s.join('-')
                };
              }
            },
            {
              config: 'project',
              type: 'input',
              message: 'Project name',
              validate: function(value) {
                if (value === '') {
                  return 'Please enter a project name';
                }
                return true;
              },
              filter:  function(value) {
                // Return ProjectName, Project Name and project-name
                var p = value.split(' '),
                    i = 0,
                    l = p.length,
                    n = [],
                    s = [];
                for (;i < l; i++) {
                  n[i] = p[i].substr(0,1).toUpperCase()+p[i].substr(1);
                  s[i] = p[i].toLowerCase();
                }
                return {
                  "name" : n.join(' '),
                  "title" : n.join(''),
                  "string" : s.join('-')
                };
              }
            },
            // {
            //   config: 'listAnswer',
            //   type: 'list',
            //   message: 'What do you want to drink?',
            //   default: 'beer',
            //   choices: ['beer', 'wine', 'tea', 'coffee', 'water']
            //   // validate: function(value), // return true if valid, error message if invalid
            //   // filter:  function(value), // modify the answer
            //   // when: function(answers) // only ask this question when this function returns true
            // }
          ]
        }
      },
    },

    banner: {
      compact: '/*! <%= pkg.name %> <%= pkg.version %> */',
      full: '/*!\n' +
        ' * <%= pkg.name %> v<%= pkg.version %>\n' +
        ' * Copyright (c) <%= pkg.author =%>\n' +
        ' \n' +
        '/*\n' +
        ' * This was developed and built by <%= _.pluck(pkg.contributors, "name").join(", ") %>\n' +
        ' * at Force24 on behalf of <%= pkg.client.name =%>.\n' +
        ' */'
    },

    // Server
    express: {
      dev: {
        options: {
          script: './server.js'
        }
      }
    },
    open : {
      dev : {
        path: 'http://localhost:3000'
      }
    },
    watch: {
      autoprefix: {
        files: ['src/css/screen.css'],
        tasks: ['autoprefixer']
      },
      css: {
        files: ['src/css/styles.css'],
        options: { livereload: true}
      },
      js: {
        files: ['src/js/**/*.js'],
        options: { livereload: true}
      },
      html: {
        files: ['src/**/*.html'],
        options: { livereload: true}
      }
    },

    // HTML
    processhtml: {
      stratus: {
        files: {
          'dist/index.html': ['src/index.html']
        }
      },
      dist: {
        files: {
          'dist/index.html': ['src/index.html']
        }
      }
    },
    autoprefixer: {
      single_file: {
        src: 'src/css/screen.css',
        dest: 'src/css/styles.css'
      }
    },
    cssmin: {
      compress: {
        options: {
          banner: '<%= banner.compact %>'
        },
        files: {
          "dist/css/styles.min.css": ['dist/css/styles.css']
        }
      }
    },

    // Javascript
    uglify: {
      bowerlibs: {
        files: [
          { src: 'src/js/vendor/modernizr.js', dest:'src/js/vendor/modernizr.js' },
          { src: 'src/js/vendor/require.js', dest:'src/js/vendor/require.js' }
        ]
      }
    },
    requirejs: {
      compile: {
        options: {
          baseUrl: "dist/js/",
          mainConfigFile: "dist/js/app.js",
          out: "dist/js/app.js",
          name: "app",
          paths: {
              jquery: "vendor/jquery.min"
          },
          wrap: {
            start: '<%= banner.compact %>',
            end: ''
          }
        }
      }
    },

    // Images
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'dist/img/',
          src: '**/*.{png,gif,jpg}',
          dest: 'dist/img/'
        }]
      }
    },

    copy: {
      bowerlibs: {
        files: [
          { expand:true, cwd:'bower_components/jquery/',    src:'jquery.min.js', dest: 'src/js/vendor/' },
          { expand:true, cwd:'bower_components/modernizr/', src:'modernizr.js', dest: 'src/js/vendor/' },
          { expand:true, cwd:'bower_components/requirejs/', src:'require.js', dest: 'src/js/vendor/' }
        ],
      },
      build: {
        files: [{
          expand: true,
          cwd: 'src/',
          src: '**/*',
          dest: 'dist/'
        }]
      }
    },
    // Tidy up after setup, before build and after build
    clean: {
      setup: [
        'log',
        '.git'
      ],
      predist: [
        'dist'
      ],
      dist: [
        'dist/config.rb',
        'dist/css/screen.css',
        'dist/css/styles.css',
        'dist/js/app',
        'dist/inc'
      ]
    },
    // Bump version numbers
    bump: {
      options: {
        files: ['package.json'],
        updateConfigs: ['pkg'],
        commit: false,
        createTag: false,
        push: false,
      }
    }

  });

  // Custom tasks
  grunt.loadTasks('tasks');
  grunt.registerTask('default', ['bowerInstall', 'copy:bowerlibs', 'uglify:bowerlibs']);
  grunt.registerTask('setup',   'Setting up external sites and repositories', ['prompt', 'updatePkg', 'curl', 'updateBugherd', 'clean:setup', 'bowerInstall', 'copy:bowerlibs', 'uglify:bowerlibs', 'resetGit']);
  grunt.registerTask('dev',     'Active development phase', ['bump:patch', 'express', 'autoprefixer', 'open', 'watch']);
  grunt.registerTask('build',   'General build task', ['autoprefixer', 'clean:predist', 'copy:build', 'requirejs', 'cssmin', 'imagemin']);
};
